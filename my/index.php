<?php 
include('config.php'); 
$query_parent = mysqli_query($conn,"SELECT * FROM head") or die("Query failed: ".mysql_error());
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Dynamic DropDown List</title>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            
           $("#parent_cat").change(function() {

              $.get('loadsubcat.php?parent_cat=' + $(this).val(), function(data) {
                 $("#sub_head").html(data);
                 $('#loader').slideUp(200, function() {
                    $(this).remove();
                });
             });	
          });

       });
    </script>
</head>

<body>
    <form method="post">
       <label for="head">HEAD SELECTION </label>

       <select name="parent_cat" id="parent_cat">
        <option selected="selected">--Select Head--</option>
        <?php while($row = mysqli_fetch_array($query_parent)): ?>
            <option value="<?php echo $row['id']; ?>"><?php echo $row['head_name']; ?></option>
        <?php endwhile; ?>
    </select>
    
    <br/><br/>
    
    <label>SUB HEAD</label>
    
    <select name="sub_head" id="sub_head"></select>
</form>
</body>
</html>
