-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2015 at 06:06 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dependent_list`
--

-- --------------------------------------------------------

--
-- Table structure for table `head`
--

CREATE TABLE IF NOT EXISTS `head` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`head_name` int(10) unsigned NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `head`
--

INSERT INTO `head` (`id`, `head_name`) VALUES
(1, 5400),
(2, 5500),
(3, 5600),
(4, 5700),
(5, 5800);

-- --------------------------------------------------------

--
-- Table structure for table `subhead`
--

CREATE TABLE IF NOT EXISTS `subhead` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`head_ID` int(11) NOT NULL,
	`subhead_name` int(10) unsigned NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `subhead`
--

INSERT INTO `subhead` (`id`, `head_ID`, `subhead_name`) VALUES
(1, 1, 5401),
(2, 1, 5402),
(3, 1, 5403),
(4, 2, 5501),
(5, 2, 5502),
(6, 2, 5503),
(7, 3, 5601),
(8, 3, 5602),
(9, 3, 5603),
(10, 4, 5701);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
